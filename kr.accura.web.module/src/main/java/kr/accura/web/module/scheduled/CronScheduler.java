package kr.accura.web.module.scheduled;

import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;

public class CronScheduler {
	@Scheduled(cron = "0 0/3 19 * * ?")
	public void run() {
		System.out.println("cron....." + new Date());
	}
}
