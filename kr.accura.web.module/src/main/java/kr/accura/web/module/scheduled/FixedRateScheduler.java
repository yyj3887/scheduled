package kr.accura.web.module.scheduled;

import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;

public class FixedRateScheduler {
	
	@Scheduled(fixedRate = 5000)
	public void run() throws InterruptedException {
		System.out.println("fixedRate....." + new Date());
		Thread.sleep(3000);
	}
}
